# Compose LazyList Drag Item Order

## 介绍
Compose LazyRow & LazyColumn 拖拽切换子项的位置.

修改自：https://github.com/indexer/TimeOutTask.git 

![示例](https://foruda.gitee.com/images/1702033540128997856/e1b556f6_1431018.gif "out.gif")